# Zweitris

Zweitris is a fun variant of Tetris for two players. As a special twist, the two 
playgrounds' floors connect in a way that allows the players to interfere with each other.

Players can start new games, connect to open games or watch running games.

This is an Elixir web application that showcases Phoenix LiveView and Presence.


## Build Setup

Install the basics:

    sudo apt install build-essentials curl git 

Get the source code:

    git clone --origin gitlab git@gitlab.com:kreba/zweitris.git

Install the required technologies according to the `.tool-versions` file
(using [asdf-vm](https://asdf-vm.com/#/core-manage-asdf-vm) or whatever suits you best)

* [Erlang and Elixir](https://elixir-lang.org/install.html)
  * If you install Erlang via asdf-vm, note that the [pre-install instructions](https://github.com/asdf-vm/asdf-erlang#before-asdf-install) are relevant!
  * Install the Erlang/OTP bindings to wxWidgets with `sudo apt install erlang-wx libwxgtk-webview3.0-gtk3-dev libcanberra-gtk-module`
* [NodeJS](https://nodejs.org/) with [Yarn](https://yarnpkg.com/getting-started)
* (This project does not use a database)

To enable live reloading during development, a filesystem watcher is required. Mac OS X and Windows users already have one but Linux users must install inotify-tools.
Please consult the [inotify-tools wiki](https://github.com/rvoicilas/inotify-tools/wiki) for distribution-specific installation instructions.

    sudo apt install inotify-tools

Install Elixir and JavaScript dependencies with

    mix setup

See whether everything compiles and works as intended by running the tests with

    mix t

Start the Phoenix endpoint with

    mix s

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.


## Production Setup

### We use Heroku

This application is currently deployed on Heroku at
[zweitris.herokuapp.com](https://zweitris-int.herokuapp.com/).

While these environments can be managed through a nice web interface,
certain tasks like introspecting an environment, accessing the database and running scripts
require the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli).

    sudo snap install heroku
    heroku login

### Deployment

Deploying new source code is as simple as a git push.
Heroku provides a git repository for the app. Add it to your local clone:

    git remote add heroku https://git.heroku.com/zweitris.git

Then you can deploy like so:

    git push int my-local-branch:master 


## Credits

Authors:
- Raffael Krebs (kreba)


## License

See the LICENSE file.
