// We need to import the CSS so that webpack will load it.
// The MiniCssExtractPlugin is used to separate it out into
// its own CSS file.
import "../css/app.scss"

// Webpack automatically bundles all modules in your
// entry points. Those entry points can be configured
// in "webpack.config.js".

// Import polyfills for IE11
import "mdn-polyfills/CustomEvent"
import "mdn-polyfills/String.prototype.startsWith"
import "mdn-polyfills/Array.from"
import "mdn-polyfills/NodeList.prototype.forEach"
import "mdn-polyfills/Element.prototype.closest"
import "mdn-polyfills/Element.prototype.matches"
import "child-replace-with-polyfill"
import "url-search-params-polyfill"
import "formdata-polyfill"
import "classlist-polyfill"

import topbar from "topbar"
import "../../deps/phoenix_html"
import { Socket } from "../../deps/phoenix"
import { LiveSocket } from "../../deps/phoenix_live_view"

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket("/live", Socket, {
  params: { _csrf_token: csrfToken },
})

// Show a progress bar when navigating or submitting forms with Phoenix LiveView
topbar.config({ barColors: { 0: "#29d" }, shadowColor: "rgba(0, 0, 0, .3)" })
window.addEventListener("phx:page-loading-start", (_info) => topbar.show())
window.addEventListener("phx:page-loading-stop", (_info) => topbar.hide())

// Connect if there are any LiveViews on the page
liveSocket.connect()

// Print debug statements to the JavaScript console
liveSocket.enableDebug()

// Simulate a bad internet connection (stays enabled for the duration of the browser session)
// liveSocket.enableLatencySim(1000)
// liveSocket.disableLatencySim()

// Expose liveSocket on window for web console debug logs and latency simulation:
window.liveSocket = liveSocket
