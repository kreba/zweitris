use Mix.Config

# Loading all available locales can be slow.
# Only load these two in order to reduce compilation times for testing.
config :zweitris, ZweitrisWeb.Gettext, allowed_locales: ["de", "en"], default_locale: "en"

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :zweitris, ZweitrisWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
