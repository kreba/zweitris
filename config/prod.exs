use Mix.Config

config :zweitris, ZweitrisWeb.Endpoint,
  http: [port: System.get_env("PORT"), transport_options: [socket_opts: [:inet6]]],
  url: [scheme: "https", host: System.get_env("HOST"), port: 443],
  force_ssl: [rewrite_on: [:x_forwarded_proto]],
  cache_static_manifest: "priv/static/cache_manifest.json"

# Do not print debug messages in production
config :logger, level: :info
