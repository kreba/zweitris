defmodule Zweitris do
  @moduledoc """
  The Zweitris namespace keeps the domain and business logic.
  """

  @spec new_game() :: {:ok, String.t()}
  @spec new_game(game_id :: String.t()) :: {:ok, String.t()}
  def new_game(game_id \\ Zweitris.GameID.random_game_id()) when is_binary(game_id) do
    {:ok, _pid} =
      DynamicSupervisor.start_child(Zweitris.GamesSupervisor, {Zweitris.GameServer, game_id})

    {:ok, game_id}
  end

  @spec all_game_ids() :: [String.t()]
  def all_game_ids() do
    DynamicSupervisor.which_children(Zweitris.GamesSupervisor)
    |> Enum.map(fn {_, pid, _, _} -> pid end)
    |> Enum.map(&Zweitris.GameID.game_id/1)
  end
end
