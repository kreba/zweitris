defmodule ZweitrisWeb.GameBoardLive do
  @moduledoc false

  use ZweitrisWeb, :live_component

  @impl true
  def update(assigns, socket) do
    %{
      id: id,
      board: board,
      yin_floater: yin_floater,
      yang_floater: yang_floater,
      perspective: perspective
    } = assigns

    %{cells_per_line: cells_per_line, lines_per_player: lines_per_player} = board

    cells =
      board
      |> Zweitris.GameBoard.merge(yin_floater, :yin)
      |> Zweitris.GameBoard.merge(yang_floater, :yang)
      |> Map.fetch!(:cells)

    cells_with_index =
      Enum.zip(
      cells,
      (for x <- 1..lines_per_player * 2, y <- 1..cells_per_line, do: {x - 1, y - 1})
    )

    {:ok,
     assign(socket,
       id: id,
       cells_with_index: cells_with_index,
       cells_per_line: cells_per_line,
       lines_per_player: lines_per_player,
       perspective: perspective
     )}
  end
end
