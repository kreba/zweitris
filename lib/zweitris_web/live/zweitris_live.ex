defmodule ZweitrisWeb.ZweitrisLive do
  @moduledoc false

  use ZweitrisWeb, :live_view

  #############################################################################
  ## INITIALIZATION

  @impl Phoenix.LiveView
  def mount(_params, _session, socket) do
    send(self(), :after_mount)
    {:ok, assign(socket, initial_status())}
  end

  defp initial_status() do
    %{
      presences: [],
      game_ids: [],
      keypress: [],
      message: nil,
      player_status: :lobby,
      my_game_id: nil,
      my_game_status: nil,
      my_side: nil
    }
  end

  defp after_mount_impl(socket) do
    player_id = socket.id

    presence_metadata = %{
      online_at: inspect(System.system_time(:second))
    }

    :ok = ZweitrisWeb.Endpoint.subscribe("player:#{player_id}")

    # By adding our presence first, before subscribing to changes and listing all known presences,
    # we avoid processing our own joining as a "presence_diff" event.
    {:ok, _ref} = ZweitrisWeb.Presence.track(self(), "players", player_id, presence_metadata)
    :ok = ZweitrisWeb.Endpoint.subscribe("players")
    presences_including_self = ZweitrisWeb.Presence.list("players")

    assign(socket, presences: presences_including_self)
  end

  #############################################################################
  ## RENDER HELPERS

  def render_winner({:game_over, my_side}, my_side), do: "You win!"
  def render_winner({:game_over, winner}, _), do: "#{winner} wins." |> String.capitalize()
  def render_winner(_, _), do: ""

  #############################################################################
  ## EVENT HANDLING

  def start_solo_game_impl(%{assigns: %{player_status: player_status}, id: player_id} = socket)
      when player_status in [:lobby, :game_over] do
    :ok = Zweitris.Matchmaker.start_solo_game(player_id)
    assign(socket, player_status: :waiting_for_opponent, message: nil)
  end

  def request_opponent_impl(%{assigns: %{player_status: player_status}, id: player_id} = socket)
      when player_status in [:lobby, :game_over] do
    :ok = Zweitris.Matchmaker.request_opponent(player_id)
    assign(socket, player_status: :waiting_for_opponent, message: nil)
  end

  def request_opponent_impl(socket) do
    socket
  end

  def cancel_opponent_request_impl(
        %{assigns: %{player_status: :waiting_for_opponent}, id: player_id} = socket
      ) do
    :ok = Zweitris.Matchmaker.cancel_opponent_request(player_id)
    assign(socket, player_status: :lobby, my_game_id: nil)
  end

  def quit_game_impl(
        %{assigns: %{player_status: :playing, my_game_id: game_id}, id: player_id} = socket
      ) do
    :ok = Zweitris.GameServer.player_quit(game_id, player_id)
    :ok = ZweitrisWeb.Endpoint.unsubscribe("game:#{game_id}")
    assign(socket, player_status: :game_over, my_game_id: nil, my_side: nil)
  end

  def quit_game_impl(socket) do
    socket
  end

  def back_to_lobby_impl(%{assigns: %{player_status: :game_over}} = socket) do
    assign(socket, player_status: :lobby, message: nil, my_game_status: nil)
  end

  def keydown_impl(%{"key" => key}, socket) do
    key = String.downcase(key)
    {is_new, new_keypress} = add_key_unique(socket.assigns.keypress, key)

    if is_new do
      socket
      |> handle_input_change(key, :start)
      |> assign(keypress: new_keypress)
    else
      socket
    end
  end

  def keyup_impl(%{"key" => "CapsLock"} = keypress, socket) do
    keyup_impl(%{keypress | "key" => "Shift"}, socket)
  end

  def keyup_impl(%{"key" => key}, socket) do
    key = String.downcase(key)
    {was_present, new_keypress} = remove_key_once(socket.assigns.keypress, key)

    if was_present do
      socket
      |> handle_input_change(key, :stop)
      |> assign(keypress: new_keypress)
    else
      socket
    end
  end

  def keyup_impl(_keypress, socket) do
    Enum.each(socket.assigns.keypress, fn key -> handle_input_change(socket, key, :stop) end)
    assign(socket, keypress: [])
  end

  defp add_key_unique([], key), do: {true, [key]}
  defp add_key_unique([key | _tail] = list, key), do: {false, list}

  defp add_key_unique([head | tail] = list, key) do
    case add_key_unique(tail, key) do
      {true, new_tail} -> {true, [head | new_tail]}
      {false, _} -> {false, list}
    end
  end

  defp remove_key_once([] = list, _key), do: {false, list}
  defp remove_key_once([key | tail], key), do: {true, tail}

  defp remove_key_once([head | tail] = list, key) do
    case remove_key_once(tail, key) do
      {true, new_tail} -> {true, [head | new_tail]}
      {false, _} -> {false, list}
    end
  end

  defp handle_input_change(socket, key, ss) when ss in [:start, :stop] do
    my_side = socket.assigns.my_side
    {acting_side, action} = keymap(my_side, key, ss)

    if socket.assigns.my_game_id && action do
      socket |> send_player_input(acting_side, action)
    else
      socket
    end
  end

  defp keymap(side, key, start_stop)
  defp keymap(_side, "p", :start), do: {nil, "toggle_pause"}
  defp keymap(:both, "e", ss), do: {:yin, "#{ss}_sliding_left"}
  defp keymap(:both, "f", ss), do: {:yin, "#{ss}_sliding_right"}
  defp keymap(:both, "r", ss), do: {:yin, "#{ss}_dropping"}
  defp keymap(:both, "d", :start), do: {:yin, "rotate"}
  defp keymap(:both, "h", ss), do: {:yang, "#{ss}_sliding_left"}
  defp keymap(:both, "i", ss), do: {:yang, "#{ss}_sliding_right"}
  defp keymap(:both, "u", ss), do: {:yang, "#{ss}_dropping"}
  defp keymap(:both, "j", :start), do: {:yang, "rotate"}
  defp keymap(:both, _key, _ss), do: {nil, nil}
  defp keymap(side, "a", ss), do: {side, "#{ss}_sliding_left"}
  defp keymap(side, "d", ss), do: {side, "#{ss}_sliding_right"}
  defp keymap(side, "w", ss), do: {side, "#{ss}_dropping"}
  defp keymap(side, "s", :start), do: {side, "rotate"}
  defp keymap(_side, _key, _ss), do: {nil, nil}

  defp send_player_input(%{assigns: %{my_game_id: game_id}} = socket, side, action) do
    Zweitris.GameServer.player_input(game_id, side, action)
    socket
  end

  #############################################################################
  ## PUBSUB HANDLING

  def presence_diff_impl(diff, socket) do
    assign(socket, :presences, update_presences(socket.assigns.presences, diff))
  end

  defp update_presences(_presences, %{joins: _joins, leaves: _leaves}) do
    # We *could* apply the diff to the existing array for a performance boost and we *should* if we ever enrich the
    # metadata using a Presence.fetch callback. See Phoenix.Presence.group for inspiration. But quick and dirty:
    ZweitrisWeb.Presence.list("players")
  end

  def player_joined_impl(game_id, game_status, side, socket) when side in [:yin, :yang, :both] do
    :ok = ZweitrisWeb.Endpoint.subscribe("game:#{game_id}")

    assign(socket,
      player_status: :playing,
      my_game_id: game_id,
      my_game_status: game_status,
      my_side: side
    )
  end

  def game_over_impl(game_id, reason, socket) do
    :ok = ZweitrisWeb.Endpoint.unsubscribe("game:#{game_id}")
    assign(socket, player_status: :game_over, message: reason, my_game_id: nil, my_side: nil)
  end

  #############################################################################
  ## TERMINATION

  @impl Phoenix.LiveView
  def terminate(_reason, %{id: player_id, assigns: %{my_game_id: game_id}})
      when not is_nil(game_id) do
    :ok = Zweitris.GameServer.player_quit(game_id, player_id)
    :ok = Zweitris.Matchmaker.cancel_opponent_request(player_id)
  end

  def terminate(_reason, _socket) do
    :ok
  end

  #############################################################################
  ## ROUTER

  @impl Phoenix.LiveView
  def handle_event(event, payload, socket)

  def handle_event("start_solo_game", _, socket) do
    {:noreply, start_solo_game_impl(socket)}
  end

  def handle_event("request_opponent", _, socket) do
    {:noreply, request_opponent_impl(socket)}
  end

  def handle_event("cancel_opponent_request", _, socket) do
    {:noreply, cancel_opponent_request_impl(socket)}
  end

  def handle_event("quit_game", _, socket) do
    {:noreply, quit_game_impl(socket)}
  end

  def handle_event("back_to_lobby", _, socket) do
    {:noreply, back_to_lobby_impl(socket)}
  end

  def handle_event("keydown", keypress, socket) do
    {:noreply, keydown_impl(keypress, socket)}
  end

  def handle_event("keyup", keypress, socket) do
    {:noreply, keyup_impl(keypress, socket)}
  end

  @impl Phoenix.LiveView
  def handle_info(payload, socket)

  def handle_info(:after_mount, socket) do
    {:noreply, after_mount_impl(socket)}
  end

  def handle_info(
        %Phoenix.Socket.Broadcast{topic: "players", event: "presence_diff", payload: diff},
        socket
      ) do
    {:noreply, presence_diff_impl(diff, socket)}
  end

  def handle_info(
        %Phoenix.Socket.Broadcast{
          topic: "player:" <> player_id,
          event: "joined",
          payload: %{game_id: game_id, game_status: game_status, side: side}
        },
        %{id: player_id} = socket
      ) do
    {:noreply, player_joined_impl(game_id, game_status, side, socket)}
  end

  def handle_info(%Phoenix.Socket.Broadcast{event: "player_input:" <> _action}, socket) do
    {:noreply, socket}
  end

  def handle_info(
        %Phoenix.Socket.Broadcast{
          topic: "game:" <> _game_id,
          event: "status_update",
          payload: game_status
        },
        socket
      ) do
    {:noreply, assign(socket, my_game_status: game_status)}
  end

  def handle_info(
        %Phoenix.Socket.Broadcast{
          topic: "game:" <> game_id,
          event: "game_over",
          payload: %{reason: reason}
        },
        socket
      ) do
    {:noreply, game_over_impl(game_id, reason, socket)}
  end
end
