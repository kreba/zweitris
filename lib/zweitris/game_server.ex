defmodule Zweitris.GameServer do
  @moduledoc false

  @registry Zweitris.GamesRegistry

  use GenServer, restart: :temporary

  alias Zweitris.{GameBoard, Tetrimino}

  #############################################################################
  ## INITIALIZATION

  # A function with this signature is expected in child_spec/1 as defined in GenServer.__using__
  def start_link(game_id) when is_binary(game_id) do
    GenServer.start_link(__MODULE__, game_id, name: via(game_id))
  end

  @impl GenServer
  def init(game_id) when is_binary(game_id) do
    {:ok, initial_state(game_id)}
  end

  defp initial_state(game_id) do
    %{
      game_id: game_id,
      game: Zweitris.GameStatus.new()
    }
  end

  #############################################################################
  ## API DECLARATION

  @spec player_join(game_id :: String.t(), player_id :: String.t(), side :: Atom.t()) :: :ok
  @spec player_quit(game_id :: String.t(), player_id :: String.t()) :: :ok
  @spec start(game_id :: String.t()) :: :ok
  @spec tick(game_id :: String.t()) :: :ok
  @spec player_input(game_id :: String.t(), player_id :: String.t(), action :: String.t()) :: :ok
  @spec game_over(game_id :: String.t(), reason :: String.t()) :: :ok

  #############################################################################
  ## API IMPLEMENTATION

  def player_join(game_id, player_id, side) when side in [:yin, :yang, :both],
    do: send_to(game_id, {:player_join, player_id, side})

  defp player_join_impl(player_id, side, %{game_id: game_id, game: game} = state) do
    subscribe("player:#{player_id}")

    broadcast_except_self("player:#{player_id}", "joined", %{
      game_id: game_id,
      game_status: game,
      side: side
    })

    {:noreply, state}
  end

  def player_quit(game_id, player_id), do: send_to(game_id, {:player_quit, player_id})

  defp player_quit_impl(player_id, state) do
    game_over_impl("Player #{player_id} quit the game", state)
  end

  def start(game_id), do: send_to(game_id, {:start})

  defp start_impl(%{game_id: game_id, game: game} = state) do
    :timer.send_interval(Zweitris.Mover.duration(:tick), self(), {:tick})
    new_game = %{game | status: :ticking}
    subscribe("game:#{game_id}")
    broadcast_except_self("game:#{game_id}", "status_update", new_game)
    {:noreply, %{state | game: new_game}}
  end

  def tick(game_id), do: send_to(game_id, {:tick})

  defp tick_impl(%{game_id: game_id, game: game} = state) do
    new_game = Zweitris.Mover.tick(game)
    broadcast_except_self("game:#{game_id}", "status_update", new_game)
    new_state = %{state | game: new_game}

    case new_game.status do
      {:game_over, _winner} -> game_over_impl("Boom.", new_state)
      _ -> {:noreply, new_state}
    end
  end

  def player_input(game_id, side, action),
    do: send_to(game_id, {:player_input, side, action})

  defp player_input_impl(side, "start_dropping", state) do
    {:noreply, state |> update_player(side, &%{&1 | dropping: true})}
  end

  defp player_input_impl(side, "stop_dropping", state) do
    {:noreply, state |> update_player(side, &%{&1 | dropping: false})}
  end

  defp player_input_impl(side, "start_sliding_left", state) do
    {:noreply, state |> update_player(side, &%{&1 | sliding_left: true})}
  end

  defp player_input_impl(side, "stop_sliding_left", state) do
    {:noreply, state |> update_player(side, &%{&1 | sliding_left: false})}
  end

  defp player_input_impl(side, "start_sliding_right", state) do
    {:noreply, state |> update_player(side, &%{&1 | sliding_right: true})}
  end

  defp player_input_impl(side, "stop_sliding_right", state) do
    {:noreply, state |> update_player(side, &%{&1 | sliding_right: false})}
  end

  defp player_input_impl(side, "rotate", %{game: game} = state) do
    player = if side == :yin, do: game.yin, else: game.yang
    rotated_floater = Tetrimino.rotate(player.floater)

    if GameBoard.collides?(game.board, rotated_floater, side) ||
         GameBoard.out_of_bounds?(game.board, rotated_floater) do
      {:noreply, state}
    else
      {:noreply, state |> update_player(side, &%{&1 | floater: rotated_floater})}
    end
  end

  defp player_input_impl(_player_id, "toggle_pause", %{game_id: game_id, game: game} = state) do
    new_game = Zweitris.Mover.toggle_pause(game)
    broadcast_except_self("game:#{game_id}", "status_update", new_game)
    {:noreply, %{state | game: new_game}}
  end

  def game_over(game_id, reason), do: send_to(game_id, {:game_over, reason})

  defp game_over_impl(reason, %{game_id: game_id} = _state) do
    broadcast_except_self("game:#{game_id}", "game_over", %{reason: reason})
    exit(:normal)
  end

  #############################################################################
  ## TERMINATION

  @impl GenServer
  def terminate(:normal, _state) do
  end

  def terminate(_reason, %{game_id: game_id} = _state) do
    ZweitrisWeb.Endpoint.broadcast("game:#{game_id}", "game_over", %{
      reason: "The game terminated abnormally. Sorry!"
    })
  end

  #############################################################################
  ## SHARED HELPER FUNCTIONS

  defp send_to(game_id, request) when is_binary(game_id) do
    send(pid(game_id), request)
    :ok
  end

  defp via(game_id) when is_binary(game_id) do
    {:via, Registry, {@registry, game_id}}
  end

  defp pid(game_id) when is_binary(game_id) do
    case Registry.lookup(@registry, game_id) do
      [] -> nil
      [{pid, _novalue}] -> pid
    end
  end

  defp update_player(%{game_id: game_id, game: game} = state, side, f) do
    player = Map.get(game, side)
    new_game = Map.put(game, side, f.(player))
    broadcast_except_self("game:#{game_id}", "status_update", new_game)
    %{state | game: new_game}
  end

  defp subscribe(topic), do: ZweitrisWeb.Endpoint.subscribe(topic)

  defp broadcast_except_self(topic, event, msg) do
    ZweitrisWeb.Endpoint.broadcast_from(self(), topic, event, msg)
  end

  #############################################################################
  ## ROUTER

  @impl GenServer
  def handle_info(payload, state)

  def handle_info({:player_join, player_id, side}, state),
    do: player_join_impl(player_id, side, state)

  def handle_info({:player_quit, player_id}, state), do: player_quit_impl(player_id, state)
  def handle_info({:start}, state), do: start_impl(state)
  def handle_info({:tick}, state), do: tick_impl(state)

  def handle_info({:player_input, side, action}, state),
    do: player_input_impl(side, action, state)

  def handle_info({:game_over, reason}, state), do: game_over_impl(reason, state)
end
