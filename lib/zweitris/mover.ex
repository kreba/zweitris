defmodule Zweitris.Mover do
  @moduledoc false

  alias Zweitris.{GameBoard, GameStatus, Tetrimino}

  def direction(%{side: :yang}), do: -1
  def direction(%{side: :yin}), do: 1

  def duration(:tick), do: 50
  def duration(:slide), do: 100
  def duration(:drop), do: 100
  def duration(:fall), do: 550

  def tick(%GameStatus{status: :ticking} = game) do
    game
    |> periodically_move_floaters()
    |> check_win_condition()
  end

  def tick(game), do: game

  def toggle_pause(%GameStatus{status: :ticking} = game) do
    %{game | status: :paused}
  end

  def toggle_pause(%GameStatus{status: :paused} = game) do
    %{game | status: :ticking}
  end

  defp periodically_move_floaters(game) do
    %{
      time_until_slide: time_until_slide,
      time_until_drop: time_until_drop,
      time_until_fall: time_until_fall
    } = game

    {game, time_until_slide} =
      move_every(game, time_until_slide, duration(:slide), &delta_for_sliding/1)

    {game, time_until_drop} =
      move_every(game, time_until_drop, duration(:drop), &delta_for_dropping/1)

    {game, time_until_fall} =
      move_every(game, time_until_fall, duration(:fall), &delta_for_falling/1)

    %{
      game
      | time_until_slide: time_until_slide,
        time_until_drop: time_until_drop,
        time_until_fall: time_until_fall
    }
  end

  defp move_every(game, timer, interval, f_delta) do
    remaining = timer - duration(:tick)

    if remaining <= 0 do
      {maybe_move_floaters(game, f_delta), remaining + interval}
    else
      {game, remaining}
    end
  end

  defp maybe_move_floaters(game, f_delta) do
    %{
      board: board,
      yang: yang,
      yin: yin
    } = game

    {yang, maybe_merge_yang_floater} = move_floater(board, yang, f_delta.(yang))
    {yin, maybe_merge_yin_floater} = move_floater(board, yin, f_delta.(yin))

    board =
      GameBoard.merge_and_remove_lines(board, maybe_merge_yang_floater, maybe_merge_yin_floater)

    %{game | board: board, yang: yang, yin: yin}
  end

  defp move_floater(_board, player, {0, 0}), do: {player, nil}

  defp move_floater(board, player, {0, dy}) do
    moved_floater = player.floater |> Tetrimino.move({0, dy})

    if GameBoard.collides?(board, moved_floater, player.side) do
      stuck_floater = player.floater
      next_floater = Tetrimino.random() |> Tetrimino.to_start_position(board, player)
      player = %{player | floater: next_floater}
      {player, stuck_floater}
    else
      player = %{player | floater: moved_floater}
      {player, nil}
    end
  end

  defp move_floater(board, player, {dx, 0}) do
    moved_floater = player.floater |> Tetrimino.move({dx, 0})

    if GameBoard.collides?(board, moved_floater, player.side) ||
         GameBoard.out_of_bounds?(board, moved_floater) do
      {player, nil}
    else
      player = %{player | floater: moved_floater}
      {player, nil}
    end
  end

  defp delta_for_sliding(%{sliding_left: true, sliding_right: false} = player),
    do: {-direction(player), 0}

  defp delta_for_sliding(%{sliding_left: false, sliding_right: true} = player),
    do: {direction(player), 0}

  defp delta_for_sliding(_), do: {0, 0}

  defp delta_for_dropping(%{dropping: true} = player), do: {0, direction(player)}
  defp delta_for_dropping(_), do: {0, 0}

  defp delta_for_falling(%{dropping: false} = player), do: {0, direction(player)}
  defp delta_for_falling(_), do: {0, 0}

  defp check_win_condition(game) do
    cond do
      GameBoard.collides?(game.board, game.yin.floater, :yin) ->
        %{game | status: {:game_over, :yang}}

      GameBoard.collides?(game.board, game.yang.floater, :yang) ->
        %{game | status: {:game_over, :yin}}

      true ->
        game
    end
  end
end
