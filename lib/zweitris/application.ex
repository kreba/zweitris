defmodule Zweitris.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      ZweitrisWeb.Telemetry,
      # Start the PubSub system
      {Phoenix.PubSub, [name: Zweitris.PubSub, adapter: Phoenix.PubSub.PG2]},
      # Start the Endpoint (http/https)
      ZweitrisWeb.Endpoint,
      # Start the presence tracking facilities
      ZweitrisWeb.Presence,
      # Start the game servers supervisor
      {DynamicSupervisor, strategy: :one_for_one, name: Zweitris.GamesSupervisor},
      # Start the local game servers registry
      {Registry, keys: :unique, name: Zweitris.GamesRegistry},
      # Start the process that assigns players to games
      Zweitris.Matchmaker
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Zweitris.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    ZweitrisWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
