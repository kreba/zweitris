defmodule Zweitris.Tetrimino do
  @moduledoc false

  def t, do: [{1, 2}, {2, 3}, {2, 2}, {2, 1}]
  def i, do: [{1, 1}, {1, 2}, {1, 3}, {1, 4}]
  def o, do: [{1, 1}, {2, 1}, {1, 2}, {2, 2}]
  def j, do: [{1, 3}, {2, 3}, {2, 2}, {2, 1}]
  def l, do: [{1, 1}, {1, 2}, {1, 3}, {2, 3}]
  def s, do: [{1, 1}, {1, 2}, {2, 2}, {2, 3}]
  def z, do: [{2, 1}, {2, 2}, {1, 2}, {1, 3}]

  def random, do: Enum.random([t(), i(), o(), j(), l(), s(), z()])

  def rotate(tetrimino, degrees \\ 90)
  def rotate(tetrimino, -90), do: tetrimino |> rotate(90) |> rotate(90) |> rotate(90)
  def rotate(tetrimino, 180), do: tetrimino |> rotate(90) |> rotate(90)

  def rotate(tetrimino, 90) do
    {x_min, x_max} = tetrimino |> Enum.map(fn {x, _} -> x end) |> Enum.min_max()
    y_min = tetrimino |> Enum.map(fn {_, y} -> y end) |> Enum.min()

    tetrimino
    |> Enum.map(fn cell ->
      {x, y} = cell
      {rel_x_old, rel_y_old} = {x - x_min, y - y_min}
      {rel_x_new, rel_y_new} = {x_max - x_min - rel_y_old, rel_x_old}
      {dx, dy} = {rel_x_new - rel_x_old, rel_y_new - rel_y_old}
      {x + dx, y + dy}
    end)
  end

  def move(tetrimino, {dx, dy}) do
    tetrimino |> Enum.map(fn {x, y} -> {x + dx, y + dy} end)
  end

  def to_start_position(
        tetrimino,
        %{cells_per_line: cells_per_line, lines_per_player: lines_per_player},
        %{side: :yang}
      ) do
    tetrimino
    |> move({floor(cells_per_line / 2 - 1), lines_per_player * 2 - 3})
    |> rotate(90)
  end

  def to_start_position(tetrimino, %{cells_per_line: cells_per_line}, %{side: :yin}) do
    tetrimino
    |> move({floor(cells_per_line / 2 - 1), -1})
    |> rotate(-90)
  end

  def inspect(tetrimino) do
    for y <- 0..5, x <- -2..4 do
      tetrimino
      |> Enum.any?(fn cell -> cell == {x, y} end)
      |> case do
        true -> "◾"
        false -> "◽"
      end
    end
    |> Enum.chunk_every(7)
    |> Enum.map(&Enum.join(&1, ""))
    |> Enum.join("\n")
  end
end
