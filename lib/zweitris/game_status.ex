defmodule Zweitris.GameStatus do
  @moduledoc false

  alias Zweitris.{GameBoard, Tetrimino}

  defstruct time_until_drop: 0,
            time_until_fall: 0,
            time_until_slide: 0,
            status: :paused,
            board: %GameBoard{},
            yang: %{
              side: :yang,
              floater: nil,
              dropping: false,
              sliding_left: false,
              sliding_right: false
            },
            yin: %{
              side: :yin,
              floater: nil,
              dropping: false,
              sliding_left: false,
              sliding_right: false
            }

  def new(lines_per_player \\ 18, cells_per_line \\ 11) do
    game = %__MODULE__{}
    board = GameBoard.new(lines_per_player, cells_per_line)

    %{
      game
      | board: board,
        yang: initial_player(board, game.yang),
        yin: initial_player(board, game.yin)
    }
  end

  defp initial_player(board, player) do
    %{player | floater: Tetrimino.t() |> Tetrimino.to_start_position(board, player)}
  end
end
