defmodule Zweitris.GameID do
  @moduledoc false

  def game_id(pid) when is_pid(pid) do
    GenServer.call(pid, :game_id)
  end

  @chars Enum.to_list(?A..?Z) ++ Enum.to_list(?a..?z)
  def random_game_id() do
    @chars
    |> Enum.take_random(8)
    |> List.to_string()
    |> upcase_first()
  end

  # No, sadly that's not the same as String.capitalize/1
  defp upcase_first(<<first>> <> rest, mode \\ :default) do
    String.upcase(<<first>>, mode) <> rest
  end
end
