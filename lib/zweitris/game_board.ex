defmodule Zweitris.GameBoard do
  @moduledoc false

  defstruct lines_per_player: 0,
            cells_per_line: 0,
            cells: []

  def new(lines_per_player, cells_per_line) do
    cells_per_player = lines_per_player * cells_per_line
    yin_cells = List.duplicate(:yin, cells_per_player)
    yang_cells = List.duplicate(:yang, cells_per_player)

    %__MODULE__{
      lines_per_player: lines_per_player,
      cells_per_line: cells_per_line,
      cells: yang_cells ++ yin_cells
    }
  end

  def collides?(board, floater, side) when side in [:yin, :yang] do
    Enum.any?(floater, fn {x, y} ->
      board_cell = board.cells |> Enum.at(cell_index(x, y, board.cells_per_line))
      board_cell == side
    end)
  end

  def out_of_bounds?(board, floater) do
    Enum.any?(floater, fn {x, _y} ->
      x < 0 || x >= board.cells_per_line
    end)
  end

  def merge(%__MODULE__{} = board, nil, _side), do: board

  def merge(%__MODULE__{} = board, floater, side) when side in [:yin, :yang] do
    merged_cells =
      Enum.reduce(floater, board.cells, fn {x, y}, result ->
        List.replace_at(result, cell_index(x, y, board.cells_per_line), side)
      end)

    %{board | cells: merged_cells}
  end

  def merge_and_remove_lines(board, yang_cells, yin_cells) do
    board
    |> merge(yang_cells, :yang)
    |> merge(yin_cells, :yin)
    |> remove_completed_lines(yang_cells, :yang)
    |> remove_completed_lines(yin_cells, :yin)
  end

  def remove_completed_lines(board, maybe_floater, side)
  def remove_completed_lines(board, nil, _), do: board

  def remove_completed_lines(
        %{cells_per_line: cells_per_line, cells: board_cells} = board,
        floater,
        side
      ) do
    board_lines = Enum.chunk_every(board_cells, cells_per_line)
    affected_line_indices = floater |> Enum.map(fn {_x, y} -> y end) |> Enum.uniq()

    completed_line_indices =
      affected_line_indices
      |> Enum.filter(fn y ->
        affected_line = Enum.at(board_lines, y)
        affected_line |> Enum.all?(fn cell -> cell == side end)
      end)

    lines_except_completed =
      completed_line_indices
      |> Enum.sort(&>=/2)
      |> Enum.reduce(board_lines, fn y, lines -> lines |> List.delete_at(y) end)

    new_board_lines =
      case side do
        :yang ->
          replacement_lines =
            :yin
            |> List.duplicate(cells_per_line)
            |> List.duplicate(length(completed_line_indices))

          lines_except_completed ++ replacement_lines

        :yin ->
          replacement_lines =
            :yang
            |> List.duplicate(cells_per_line)
            |> List.duplicate(length(completed_line_indices))

          replacement_lines ++ lines_except_completed
      end

    %{board | cells: List.flatten(new_board_lines)}
  end

  def inspect(%__MODULE__{} = board) do
    %{cells_per_line: cells_per_line, cells: cells} = board

    cells
    |> Enum.map(fn cell ->
      case cell do
        :yin -> "◽"
        :yang -> "◾"
      end
    end)
    |> Enum.chunk_every(cells_per_line)
    |> Enum.map(&Enum.join(&1, ""))
    |> Enum.join("\n")
  end

  defp cell_index(x, y, cells_per_line), do: y * cells_per_line + x
end
