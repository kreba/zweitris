defmodule Zweitris.Matchmaker do
  @moduledoc false

  use GenServer

  #############################################################################
  ## INITIALIZATION

  # A function with this signature is expected in child_spec/1 as defined in GenServer.__using__
  def start_link([]) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  def init(_) do
    Process.send_after(self(), :connect, 1000)
    {:ok, initial_state()}
  end

  defp initial_state() do
    %{
      players_looking_for_opponent: []
    }
  end

  #############################################################################
  ## API DECLARATION

  @spec request_opponent(String.t()) :: :ok
  @spec cancel_opponent_request(String.t()) :: :ok

  #############################################################################
  ## API IMPLEMENTATION

  def start_solo_game(player_id) do
    ZweitrisWeb.Endpoint.broadcast_from(self(), "games:matchmaking", "start_solo_game", %{
      player_id: player_id
    })
  end

  def start_solo_game_impl(player_id, state) do
    {:ok, game_id} = Zweitris.new_game()

    Zweitris.GameServer.player_join(game_id, player_id, :both)
    Zweitris.GameServer.start(game_id)

    {:noreply, state}
  end

  def request_opponent(player_id) do
    ZweitrisWeb.Endpoint.broadcast_from(self(), "games:matchmaking", "request_opponent", %{
      player_id: player_id
    })
  end

  def request_opponent_impl(player_id, %{players_looking_for_opponent: []} = state) do
    {:noreply, %{state | players_looking_for_opponent: [player_id]}}
  end

  def request_opponent_impl(
        player_id,
        %{players_looking_for_opponent: [opponent | other_players]} = state
      ) do
    {:ok, game_id} = Zweitris.new_game()

    Zweitris.GameServer.player_join(game_id, opponent, :yin)
    Zweitris.GameServer.player_join(game_id, player_id, :yang)
    Zweitris.GameServer.start(game_id)

    {:noreply, %{state | players_looking_for_opponent: other_players}}
  end

  def cancel_opponent_request(player_id) do
    ZweitrisWeb.Endpoint.broadcast_from(self(), "games:matchmaking", "cancel_opponent_request", %{
      player_id: player_id
    })
  end

  def cancel_opponent_request_impl(player_id, state) do
    {:noreply,
     %{state | players_looking_for_opponent: state.players_looking_for_opponent -- [player_id]}}
  end

  #############################################################################
  ## ROUTER

  def handle_info(:connect, state) do
    ZweitrisWeb.Endpoint.subscribe("games:matchmaking", link: true)
    {:noreply, state}
  end

  def handle_info(
        %Phoenix.Socket.Broadcast{event: "start_solo_game", payload: %{player_id: player_id}},
        state
      ) do
    start_solo_game_impl(player_id, state)
  end

  def handle_info(
        %Phoenix.Socket.Broadcast{event: "request_opponent", payload: %{player_id: player_id}},
        state
      ) do
    request_opponent_impl(player_id, state)
  end

  def handle_info(
        %Phoenix.Socket.Broadcast{
          event: "cancel_opponent_request",
          payload: %{player_id: player_id}
        },
        state
      ) do
    cancel_opponent_request_impl(player_id, state)
  end
end
