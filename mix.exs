defmodule Zweitris.MixProject do
  use Mix.Project

  def project do
    [
      app: :zweitris,
      version: "0.2.0",
      elixir: "~> 1.11",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:phoenix, :gettext] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      preferred_cli_env: [t: :test],
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Zweitris.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      {:credo, "~> 1.5", only: :dev},
      {:floki, ">= 0.0.0", only: :test},
      {:gettext, "~> 0.11"},
      {:jason, "~> 1.0"},
      {:phoenix, "~> 1.5"},
      {:phoenix_html, "~> 2.11"},
      {:phoenix_live_dashboard, ">= 0.0.0"},
      {:phoenix_live_reload, "~> 1.2", only: :dev},
      {:phoenix_live_view, "~> 0.15.0"},
      {:phoenix_pubsub, "~> 2.0"},
      {:plug_cowboy, "~> 2.0"},
      {:telemetry_metrics, "~> 0.4"},
      {:telemetry_poller, "~> 0.4"}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      setup: ["g"],
      c: ["cmd echo Use the bash alias iem=\\\\'iex --dot-iex .iex-mix.exs -S mix\\\\'"],
      f: ["credo", "format", "y format"],
      g: ["deps.get", "deps.compile", "y install"],
      o: ["hex.info", "hex.outdated", "y outdated || echo -n"],
      up: ["deps.update --all", "y upgrade"],
      x: ["gettext.extract", "gettext.merge priv/gettext"],
      y: ["cmd yarn --cwd=assets --color --production=false"],
      s: ["phx.server"],
      t: ["test"]
    ]
  end
end
