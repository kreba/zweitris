#!/usr/bin/env bash

echo "-----> Precompiling assets..."

# See https://ss64.com/bash/set.html
set -o errexit   # Always exit on error
set -o pipefail  # Don't ignore exit codes when piping output
set -o xtrace    # Echo all commands as they are executed

# Compile the GUI
# Yarn is provided by the heroku/nodejs buildpack.
# That buildpack also installs all dependencies for the root package.json and caches them.
mix y build --profile --color
find . -type d -name "node_modules" -exec rm -rf {} +

# Now produce the asset digests and the cache_manifest.json
mix phx.digest priv/static
